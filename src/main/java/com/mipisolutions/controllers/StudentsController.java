package com.mipisolutions.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mipisolutions.entities.Student;
import com.mipisolutions.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by chrismipi on 2015/12/15.
 */
@RestController
@RequestMapping("/students")
public class StudentsController {

	@Autowired
	private StudentService service;

	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity createUser(@RequestBody String json) {
		try {
			ObjectMapper mapper = new ObjectMapper();

			final JsonNode data = mapper.readTree(json);

			String name = data.get("firstName").asText();
			String surname = data.get("lastName").asText();

			service.saveStudent(new Student(name, surname));

			return new ResponseEntity(HttpStatus.CREATED);

		} catch (IOException e) {
			// TODO add a logger
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

	}


}
