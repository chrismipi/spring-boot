package com.mipisolutions.services;

import com.mipisolutions.entities.Student;
import com.mipisolutions.repositories.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by chrismipi on 2015/12/15.
 */
@Component
public class StudentService {

	@Autowired
	private StudentRepo repo;
	private List<Student> allStudents;

	public Student saveStudent(Student student) {
		return repo.save(student);
	}

	public void updateStudent(Student student) {
		saveStudent(student);
	}

	public Student findStudentById(final long id) {
		return repo.findOne(id);
	}

	public List<Student> getAllStudents() {
		return (List<Student>) repo.findAll();
	}
}
