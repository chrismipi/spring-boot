package com.mipisolutions.repositories;

import com.mipisolutions.entities.Student;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by chrismipi on 2015/12/15.
 */
public interface StudentRepo extends CrudRepository<Student,Long> {
}
