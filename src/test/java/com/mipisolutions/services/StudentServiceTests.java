package com.mipisolutions.services;

import com.mipisolutions.DemoApplication;
import com.mipisolutions.entities.Student;
import com.mipisolutions.repositories.StudentRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by chrismipi on 2015/12/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
public class StudentServiceTests {

	@Autowired
	private StudentRepo repo;

	@Autowired
	private StudentService service;

	@Before
	public void setUp() {
		// ID should be 1L
		Student student1 = new Student("Piet", "John");
		repo.save(student1);

		// ID should be 2L
		Student madlu = new Student("Madlu", "Phuthu");
		repo.save(madlu);
	}

	@Test
	public void saveStudent() {
		Student student = new Student("Chris", "McMipi");

		service.saveStudent(student);

		Student chris = repo.findOne(student.getId());
		// make sure that chris == student
		assertEquals(student.getFirstName(), chris.getFirstName());
	}

	@Test
	public void updateStudent() {

		// given that the student exit
		Student student = repo.findOne(1L);
		assertEquals("John", student.getLastName());

		// when updating the student
		String lastName = "Black";
		student.setLastName(lastName);

		// it should be updated
		service.updateStudent(student);

		Student updatedStudent = repo.findOne(1L);

		assertEquals(lastName, updatedStudent.getLastName());
	}

	@Test
	public void getStudentById() {
		// students exits in the db
		// we get it by ID

		Student student = service.findStudentById(2L);

		// make sure its the right student
		assertEquals("Madlu", student.getFirstName());
		assertEquals("Phuthu", student.getLastName());
	}

	@Test
	public void getAllStudents() {
		// given students exits in the db

		// get all students
		List<Student> studentList = service.getAllStudents();

		// make sure that we actually getting all of them
	}
}
