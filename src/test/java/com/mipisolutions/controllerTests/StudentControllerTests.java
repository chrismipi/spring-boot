package com.mipisolutions.controllerTests;

import com.mipisolutions.DemoApplication;
import com.mipisolutions.services.StudentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by chrismipi on 2015/12/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
public class StudentControllerTests {

	private MockMvc mockMvc;

	private MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private StudentService service;


	@Before
	public void setUp() {
		assertNotNull(webApplicationContext);
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

//		Student student = new Student("Chris", "Chris");
//		when(service.saveStudent(student)).thenReturn(student);
	}

	@Test
	public void createStudent() {

		String json = "{\"firstName\":\"Chris\", \"lastName\":\"Chris\"}";

		try {
			mockMvc.perform(put("/students")
					.contentType(APPLICATION_JSON_UTF8)
					.content(json))
					.andExpect(status().isCreated());
		} catch (Exception e) {
			fail("Reason for failing : " + e.getLocalizedMessage());
		}
	}
}
